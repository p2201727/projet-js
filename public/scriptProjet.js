// jshint browser:true, eqeqeq:true, undef:true, devel:true, esversion: 8

// Clé d'API Pexels et URL de l'API pour la recherche d'images sportives
const clef = "yF77qVKnOb8q51IITkMRXyXpkvP3nzhAW3Iu8gqgBk6iaaAEwSSwTFyx";
const url = "https://api.pexels.com/v1/search?query=sport";

// Chemins d'accès aux images locales
const img1 = "../img/img1.jpg";
const img2 = "../img/img2.jpg";
const img3 = "../img/img3.jpg";
const img4 = "../img/img4.jpg";
const img5 = "../img/img5.jpg";
const img6 = "../img/img6.jpg";

// Élément 'aside' dans le document HTML
const aside = document.querySelector('aside');

// Fonction pour afficher les images locales
function afficheImg() {
    // Création et ajout des éléments img pour les images locales
    let i1 = document.createElement('img');
    i1.src = img1;
    aside.appendChild(i1);



    let i2 = document.createElement('img');
    i2.src = img2;
    aside.appendChild(i2);

    let i3 = document.createElement('img');
    i3.src = img3;
    aside.appendChild(i3);

    let i4 = document.createElement('img');
    i4.src = img4;
    aside.appendChild(i4);

    let i5 = document.createElement('img');
    i5.src = img5;
    aside.appendChild(i5);

    let i6 = document.createElement('img');
    i6.src = img6;
    aside.appendChild(i6);
    i6.classList.add('img');
}

// Fonction asynchrone pour récupérer et afficher les images de l'API Pexels
async function imagesApi() {
    try {
        // Requête à l'API Pexels avec la clé d'authentification
        var reponse = await fetch(url, {
            headers: {
                Authorization: clef
            }
        });

        // Si la réponse est OK (status code 200), traiter les données
        if (reponse.ok) {
            const data = await reponse.json();
            const taille = data.photos.length;

            // Boucle pour créer et afficher les éléments img avec les images de l'API
            for (let i = 0; i < taille; i++) {
                let img = document.createElement('img');
                img.src = data.photos[i].src.small;
                aside.appendChild(img);
            }




            // Sélectionnez la zone de dépôt des images sources
            var posterZone = document.getElementById("poster");

            // Ajoutez un écouteur au clic pour les zones de dépôt des images sources
            posterZone.addEventListener("click", function (event) {
                var clickedZone = event.target;
                var imagesLeft = document.querySelectorAll("img");
                imagesLeft.forEach(function (imageLeft) {
                    imagesLeft.forEach(function (img) {
                        img.classList.remove("selected");
                    });
                });

                // Vérifiez si l'élément cliqué a la classe "poster-zone"
                if (clickedZone.classList.contains("poster-zone")) {
                    // Supprimez la classe 'poster-zone-selected' de toutes les zones
                    var posterZones = document.querySelectorAll(".poster-zone");
                    posterZones.forEach(function (zone) {
                        zone.classList.remove("poster-zone-selected");
                    });

                    // Ajoutez la classe 'poster-zone-selected' à la zone cliquée
                    clickedZone.classList.add("poster-zone-selected");
                }
            });



            // Sélectionnez les éléments de la partie gauche (images)
            var imagesLeft = document.querySelectorAll("img");

            // Ajoutez un écouteur au clic pour chaque image de la partie gauche
            imagesLeft.forEach(function (imageLeft) {
                imageLeft.addEventListener("click", function () {
                    // Supprimez la classe 'selected' de toutes les images de la partie gauche
                    imagesLeft.forEach(function (img) {
                        img.classList.remove("selected");
                    });

                    // Récupérez la source de l'image cliquée
                    var selectedImageSrc = imageLeft.getAttribute("src");

                    // Trouvez la zone sélectionnée
                    var selectedZone = document.querySelector(".poster-zone-selected");

                    // Si une zone est sélectionnée, remplacez le fond par l'image
                    if (selectedZone) {
                        selectedZone.style.backgroundImage = "url('" + selectedImageSrc + "')";
                    }

                    // Ajoutez la classe 'selected' à l'image cliquée
                    imageLeft.classList.add("selected");
                });
            });




            // Créez 4 zones en pointillés rouges
            for (var i = 1; i <= 4; i++) {
                var zone = document.createElement("div");
                zone.classList.add("poster-zone");
                zone.textContent = "Zone " + i;
                posterZone.appendChild(zone);
            }

        } else {
            // En cas d'erreur dans la réponse, afficher les images locales et lever une exception
            afficheImg();
            throw ("Err " + reponse.status);
        }
    } catch (err) {
        // En cas d'erreur lors de la requête, afficher les images locales
        console.log(err);
        afficheImg();
    }
}

// Appeler la fonction pour récupérer et afficher les images au chargement de la page
imagesApi();

